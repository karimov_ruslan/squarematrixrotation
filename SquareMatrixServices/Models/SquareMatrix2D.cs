﻿using System;
using SquareMatrixServices.Models.Interfaces;

namespace SquareMatrixServices.Models
{
    public class SquareMatrix2D<T> : Matrix2D<T>, ISquareMatrix2D<T>
    {
        public int Size { get; private set; }

        public SquareMatrix2D(int matrixSize) : base(matrixSize, matrixSize)
        {
            Size = matrixSize;
        }
    }
}