﻿using System;

namespace SquareMatrixServices.Models
{
    public struct Matrix2DIndex
    {
        public Matrix2DIndex(int row, int column)
        {
            Row = row;
            Column = column;
        }
        public int Row;
        public int Column;

        public bool IsWithinBounds(int lowerBound, int upperBound) =>
            Row > lowerBound && Row < upperBound &&
            Column > lowerBound && Column < upperBound;
    }
}