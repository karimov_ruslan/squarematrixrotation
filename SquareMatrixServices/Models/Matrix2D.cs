﻿using SquareMatrixServices.Models.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace SquareMatrixServices.Models
{
    public class Matrix2D<T> : IMatrix2D<T>, IEnumerable<T>
    {
        public Matrix2D(int rows, int columns)
        {
            array = new T[rows, columns];
            Rows = rows;
            Columns = columns;
        }

        public int Rows { get; private set; }
        public int Columns { get; private set; }

        public T this[Matrix2DIndex index]
        {
            get => this[index.Row, index.Column];
            set => this[index.Row, index.Column] = value;
        }

        private readonly T[,] array;

        public T this[int i, int j]
        {
            get => Get(i, j);
            set => Set(i, j, value);
        }

        public T Get(int row, int column)
        {
            //ValidateIndexes(row, column);
            return array[row, column];
        }

        public T Set(int row, int column, T value)
        {
            //ValidateIndexes(row, column);
            return array[row, column] = value;
        }

        //private void ValidateIndexes(int row, int column)
        //{
        //    if (row >= Rows)
        //        throw new IndexOutOfRangeException(nameof(row));
        //    if (column >= Columns)
        //        throw new IndexOutOfRangeException(nameof(column));
        //}

        public void Swap(Matrix2DIndex first, Matrix2DIndex second)
        {
            var temp = this[first];
            this[first] = this[second];
            this[second] = temp;
        }

        public void CopyTo(IMatrix2D<T> matrix)
        {
            for (int row = 0; row < Rows; row++)
                for (int column = 0; column < Columns; column++)
                    matrix[row, column] = this[row, column];
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            int maxCellLength = this
                .Select(x => x.ToString())
                .Max(x => x.Length);

            sb.Append("\n");

            sb.Append("|");
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    var value = array[i, j].ToString()
                        .PadRight(maxCellLength);
                    sb.Append($" {value} |");
                }

                sb.Append("\n|");
            }
            sb.Remove(sb.Length - 1, 1);

            sb.Append("\n");
            return sb.ToString();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return array.Cast<T>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return array.GetEnumerator();
        }
    }
}