﻿using System.Collections.Generic;

namespace SquareMatrixServices.Models.Interfaces
{
    public interface ISquareMatrix2D<T> : IMatrix2D<T>, IEnumerable<T>
    {
        public int Size { get; }
    }
}