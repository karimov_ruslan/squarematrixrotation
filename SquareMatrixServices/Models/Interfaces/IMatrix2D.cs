﻿namespace SquareMatrixServices.Models.Interfaces
{
    public interface IMatrix2D<T>
    {
        T this[int i, int j] { get; set; }
        T this[Matrix2DIndex index] { get; set; }

        int Columns { get; }
        int Rows { get; }

        void Swap(Matrix2DIndex first, Matrix2DIndex second);

        void CopyTo(IMatrix2D<T> destination);
    }
}