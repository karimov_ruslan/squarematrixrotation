﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SquareMatrixServices.Enums
{
    public enum API
    {
        GenerateRandomMatrixAndSaveToDataStore,
        RotateMatrixFromDataStore,
        PrintMatrixFromDataStore,
        PrintLevelsForMatrixFromDataStore
    }
}
