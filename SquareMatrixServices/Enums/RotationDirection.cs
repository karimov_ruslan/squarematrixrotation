﻿namespace SquareMatrixServices.Enums
{
    public enum RotationDirection
    {
        Clockwise,
        AntiClockwise
    }
}