﻿namespace SquareMatrixServices.Enums
{
    internal enum Direction2D
    {
        Forward,
        Backward,
        Up,
        Down
    }
}