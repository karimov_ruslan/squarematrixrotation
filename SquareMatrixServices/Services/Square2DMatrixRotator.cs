﻿using SquareMatrixServices.Enums;
using SquareMatrixServices.Models;
using SquareMatrixServices.Models.Interfaces;
using SquareMatrixServices.Services.Interfaces;
using System;
using System.Text;

namespace SquareMatrixServices.Services
{
    public class Square2DMatrixRotator : ISquare2DMatrixRotator
    {
        private IMatrixSquare2DCircleTraversal MatrixTraversal { get; }

        public Square2DMatrixRotator(IMatrixSquare2DCircleTraversal traversal)
        {
            MatrixTraversal = traversal;
        }

        public void Rotate90Degree<T>(ISquareMatrix2D<T> matrix)
        {
            RotateInternal(matrix, RotationDirection.AntiClockwise);
        }

        public void RotateMinus90Degree<T>(ISquareMatrix2D<T> matrix)
        {
            RotateInternal(matrix, RotationDirection.Clockwise);
        }

        private void RotateInternal<T>(ISquareMatrix2D<T> matrix, RotationDirection direction)
        {
            var levels = GetLevels(matrix);

            for (int level = 0; level < levels; level++)
            {
                MatrixTraversal.Initialize(level, matrix.Size, direction);
                for (int rotationNumber = 0; rotationNumber < matrix.Size - 2 * level - 1; rotationNumber++)
                {
                    var first = MatrixTraversal.Current;
                    while (MatrixTraversal.MoveNext())
                    {
                        var second = MatrixTraversal.Current;
                        matrix.Swap(first, second);

                        first = second;
                    }
                }
            }
        }

        private int GetLevels<T>(ISquareMatrix2D<T> matrix) => matrix.Size / 2;

        public string LevelsToString(ISquareMatrix2D<int> matrix, RotationDirection direction)
        {
            var levels = GetLevels(matrix);
            var sb = new StringBuilder();
            for (int level = 0; level < levels; level++)
            {
                sb.AppendLine($"Level {level}:");
                sb.AppendLine(LevelToString(matrix, level, direction));
            }
            return sb.ToString();
        }

        private string LevelToString<T>(ISquareMatrix2D<T> matrix, int level, RotationDirection direction)
        {
            MatrixTraversal.Initialize(level, matrix.Size, direction);
            var sb = new StringBuilder();
            do
            {
                int row = MatrixTraversal.Current.Row, column = MatrixTraversal.Current.Column;
                sb.Append($"{matrix[row, column]} | ");
            } while (MatrixTraversal.MoveNext());
            sb.AppendLine();

            return sb.ToString();
        }
    }
}