﻿using SquareMatrixServices.Enums;
using SquareMatrixServices.Models;
using SquareMatrixServices.Services.Interfaces;
using System;
using System.Diagnostics;
using System.Linq;

namespace SquareMatrixServices.Services
{
    public class SquareMatrixRotationApp : IApp
    {
        private IDataStore DataStore { get; }
        private IMatrixFiller<int> MatrixFiller { get; }
        private ISquare2DMatrixRotator MatrixRotator { get; }
        private IWriteService WriteService { get; }
        private IReadService ReadService { get; }

        public SquareMatrixRotationApp(IDataStore dataStore,
            IMatrixFiller<int> matrixFiller,
            ISquare2DMatrixRotator matrixRotator,
            IWriteService printer,
            IReadService reader)
        {
            DataStore = dataStore;
            MatrixFiller = matrixFiller;
            MatrixRotator = matrixRotator;
            WriteService = printer;
            ReadService = reader;
        }

        private API? SelectAPI()
        {
            WriteService.WriteLine();
            WriteService.WriteLine($"Here is the list of available for the moment functions:");
            WriteService.WriteLine();
            WriteService.WriteLine($"{(int)API.GenerateRandomMatrixAndSaveToDataStore} - {API.GenerateRandomMatrixAndSaveToDataStore}");
            WriteService.WriteLine($"{(int)API.RotateMatrixFromDataStore} - {API.RotateMatrixFromDataStore}");
            WriteService.WriteLine($"{(int)API.PrintMatrixFromDataStore} - {API.PrintMatrixFromDataStore}");
            WriteService.WriteLine($"{(int)API.PrintLevelsForMatrixFromDataStore} - {API.PrintLevelsForMatrixFromDataStore}");
            WriteService.WriteLine();

            API? result = null;
            var line = string.Empty;
            while (!result.HasValue && !line.Contains("-1"))
            {
                WriteService.WriteLine("Please, enter the number of API, you want to run (or -1 to exit):");
                line = ReadService.ReadLine();

                if (int.TryParse(line, out int apiInt) && Enum.GetValues(typeof(API)).OfType<API>().Any(x => (int)x == apiInt))
                    result = (API)apiInt;
            }

            return result;
        }

        private int ReadNaturalNumber()
        {
            while (true)
            {
                WriteService.WriteLine("Enter a natural number:");
                var line = ReadService.ReadLine();
                if (!int.TryParse(line, out int naturalNumber) || naturalNumber <= 0)
                    WriteService.WriteLine($"{line} is not a natural number!");
                else return naturalNumber;
            }
        }

        public void Startup()
        {
            WriteService.WriteLine($"{nameof(SquareMatrixServices)} console application greets you!");
            WriteService.WriteLine();

            var selectedAPI = SelectAPI();
            while (selectedAPI.HasValue)
            {
                try
                {
                    switch (selectedAPI.Value)
                    {
                        case API.GenerateRandomMatrixAndSaveToDataStore:
                            GenerateRandomMatrixAction();
                            break;
                        case API.RotateMatrixFromDataStore:
                            RotateMatrixAction();
                            break;
                        case API.PrintMatrixFromDataStore:
                            PrintMatrixFromDataStoreAction();
                            break;
                        case API.PrintLevelsForMatrixFromDataStore:
                            PrintMatrixLevelsFromDataStoreAction();
                            break;
                    }
                }
                catch(Exception ex)
                {
                    WriteService.WriteLine($"Ops. Unhandled exception in action {selectedAPI}.");
                    WriteService.WriteLine();
                    WriteService.WriteLine($"Details: {ex}");
                }
                
                selectedAPI = SelectAPI();
            }
        }

        #region Actions
        private void RotateMatrixAction()
        {
            var matrix = DataStore.Get();

            var watch = Stopwatch.StartNew();
            MatrixRotator.Rotate90Degree(matrix);
            watch.Stop();

            WriteService.WriteLine($"Time elapsed: {watch.Elapsed}");

            DataStore.Save(matrix);
        }

        private void GenerateRandomMatrixAction()
        {
            int matrixSize = ReadNaturalNumber();

            var watch = Stopwatch.StartNew();
            var matrix = new SquareMatrix2D<int>(matrixSize);
            MatrixFiller.Fill(matrix);
            watch.Stop();

            WriteService.WriteLine($"Time elapsed: {watch.Elapsed}");

            DataStore.Save(matrix);
        }

        private void PrintMatrixLevelsFromDataStoreAction()
        {
            var matrix = DataStore.Get();

            var watch = Stopwatch.StartNew();
            var levelsStr = MatrixRotator.LevelsToString(matrix, RotationDirection.AntiClockwise);
            WriteService.WriteLine(levelsStr);
            watch.Stop();

            WriteService.WriteLine($"Time elapsed: {watch.Elapsed}");
        }

        private void PrintMatrixFromDataStoreAction()
        {
            var matrix = DataStore.Get();

            var watch = Stopwatch.StartNew();
            WriteService.WriteLine(matrix.ToString());
            watch.Stop();

            WriteService.WriteLine($"Time elapsed: {watch.Elapsed}");
        }
        #endregion
    }
}