﻿using SquareMatrixServices.Services.Interfaces;
using System;

namespace SquareMatrixServices.Services
{
    public class ConsoleReader : IReadService
    {
        public string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}
