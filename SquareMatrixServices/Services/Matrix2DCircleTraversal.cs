﻿using SquareMatrixServices.Enums;
using SquareMatrixServices.Models;
using SquareMatrixServices.Services.Interfaces;
using System;

namespace SquareMatrixServices.Services
{
    public class MatrixSquare2DCircleTraversal : IMatrixSquare2DCircleTraversal
    {
        public void Initialize(int level, int size, RotationDirection direction)
        {
            var index = new Matrix2DIndex(level, level);
            First = index;
            Current = index;
            LowerBound = level - 1;
            UpperBound = size - level;
            RotationDirection = direction;

            if (direction == RotationDirection.AntiClockwise)
                CurrentDirection = Direction2D.Down;
            else CurrentDirection = Direction2D.Forward;
        }

        private RotationDirection RotationDirection { get; set; }
        private int LowerBound { get; set; }
        private int UpperBound { get; set; }
        private Direction2D CurrentDirection { get; set; }

        private Matrix2DIndex First { get; set; }
        private bool IsCurrentFirst => Current.Row == First.Row && Current.Column == First.Column;
        public Matrix2DIndex Current { get; private set; }

        public bool MoveNext()
        {
            while (true)
            {
                var newIndex = Move();
                if (!newIndex.IsWithinBounds(LowerBound, UpperBound))
                {
                    CurrentDirection = GetNext(CurrentDirection);
                    continue;
                }
                Current = newIndex;
                break;
            }
            return !IsCurrentFirst;
        }

        private Matrix2DIndex Move()
        {
            return (CurrentDirection)
            switch
            {
                (Direction2D.Down) => new Matrix2DIndex(Current.Row + 1, Current.Column),
                (Direction2D.Forward) => new Matrix2DIndex(Current.Row, Current.Column + 1),
                (Direction2D.Up) => new Matrix2DIndex(Current.Row - 1, Current.Column),
                (Direction2D.Backward) => new Matrix2DIndex(Current.Row, Current.Column - 1),
                _ => throw new Exception("Unexpected combination."),
            };
        }

        private Direction2D GetNext(Direction2D direction2D)
        {
            return (RotationDirection, direction2D)
            switch
            {
                (RotationDirection.Clockwise, Direction2D.Forward) => Direction2D.Down,
                (RotationDirection.Clockwise, Direction2D.Down) => Direction2D.Backward,
                (RotationDirection.Clockwise, Direction2D.Backward) => Direction2D.Up,
                (RotationDirection.Clockwise, Direction2D.Up) => Direction2D.Forward,
                (RotationDirection.AntiClockwise, Direction2D.Forward) => Direction2D.Up,
                (RotationDirection.AntiClockwise, Direction2D.Up) => Direction2D.Backward,
                (RotationDirection.AntiClockwise, Direction2D.Backward) => Direction2D.Down,
                (RotationDirection.AntiClockwise, Direction2D.Down) => Direction2D.Forward,
                _ => throw new Exception("Unexpected combination."),
            };
        }
    }
}