﻿using SquareMatrixServices.Services.Interfaces;
using System;

namespace SquareMatrixServices.Services
{
    public class ConsoleWriter : IWriteService
    {
        public void WriteLine(object obj = null)
        {
            Console.WriteLine(obj);
        }
    }
}
