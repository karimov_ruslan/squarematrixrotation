﻿using SquareMatrixServices.Models;
using SquareMatrixServices.Models.Interfaces;
using SquareMatrixServices.Services.Interfaces;
using System.IO;
using System.Linq;

namespace SquareMatrixServices.Services
{
    public class FileDataStore : IDataStore
    {
        private string FileName { get; set; }
        public FileDataStore(string dataStoreCustomName = null)
        {
            if (!string.IsNullOrWhiteSpace(dataStoreCustomName))
                FileName = dataStoreCustomName;
            else FileName = "DataStore.csv";
        }

        public void Save(ISquareMatrix2D<int> matrix)
        {
            using var sw = new StreamWriter(FileName);
            sw.WriteLine(matrix.Size);
            for (int row = 0; row < matrix.Size; row++)
            {
                for (int column = 0; column < matrix.Size; column++)
                    sw.Write($"{matrix[row, column]} ");
                sw.Write("\n");
            }
        }

        public ISquareMatrix2D<int> Get()
        {
            using var sr = new StreamReader(FileName);

            var sizeLine = sr.ReadLine();
            var size = int.Parse(sizeLine);
            var matrix = new SquareMatrix2D<int>(size);

            for (int row = 0; row < matrix.Size; row++)
            {
                var line = sr.ReadLine();
                var rowCols = line.Split(" ").Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x)).ToArray();
                for (int column = 0; column < matrix.Size; column++)
                    matrix[row, column] = rowCols[column];
            }

            return matrix;
        }
    }
}