﻿using SquareMatrixServices.Models.Interfaces;
using SquareMatrixServices.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SquareMatrixServices.Services
{
    public class MatrixFiller<T> : IMatrixFiller<T>
    {
        public IValuesProvider<T> ValuesProvider { get; }
        public MatrixFiller(IValuesProvider<T> provider)
        {
            ValuesProvider = provider;
        }

        public void Fill(IMatrix2D<T> matrix)
        {
            for (int i = 0; i < matrix.Rows; i++)
                for (int j = 0; j < matrix.Columns; j++)
                    matrix[i, j] = ValuesProvider.Next();
        }
    }
}