﻿using SquareMatrixServices.Services.Interfaces;
using System;

namespace SquareMatrixServices.Services
{
    public class RandomIntegersProvider : IValuesProvider<int>
    {
        private Random Random { get; }
        private int? Min { get; }
        private int? Max { get; }

        public RandomIntegersProvider()
        {
            Random = new Random();
        }

        public RandomIntegersProvider(int min, int max) : this()
        {
            Min = min;
            Max = max;
        }
        public int Next()
        {
            if(Min.HasValue && Max.HasValue)
                return Random.Next(Min.Value, Max.Value);
            return Random.Next();
        }
    }
}