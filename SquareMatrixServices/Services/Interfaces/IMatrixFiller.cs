﻿using SquareMatrixServices.Models.Interfaces;
using SquareMatrixServices.Services.Interfaces;

namespace SquareMatrixServices.Services.Interfaces
{
    public interface IMatrixFiller<T>
    {
        IValuesProvider<T> ValuesProvider { get; }

        void Fill(IMatrix2D<T> matrix);
    }
}