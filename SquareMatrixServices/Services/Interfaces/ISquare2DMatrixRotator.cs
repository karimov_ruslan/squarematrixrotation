﻿using SquareMatrixServices.Enums;
using SquareMatrixServices.Models;
using SquareMatrixServices.Models.Interfaces;

namespace SquareMatrixServices.Services.Interfaces
{
    public interface ISquare2DMatrixRotator
    {
        public string LevelsToString(ISquareMatrix2D<int> matrix, RotationDirection direction);
        public void Rotate90Degree<T>(ISquareMatrix2D<T> matrix);
        public void RotateMinus90Degree<T>(ISquareMatrix2D<T> matrix);
    }
}