﻿using SquareMatrixServices.Enums;
using SquareMatrixServices.Models;

namespace SquareMatrixServices.Services.Interfaces
{
    public interface IMatrixSquare2DCircleTraversal
    {
        Matrix2DIndex Current { get; }

        bool MoveNext();
        public void Initialize(int level, int size, RotationDirection direction);
    }
}