﻿using SquareMatrixServices.Models.Interfaces;

namespace SquareMatrixServices.Services.Interfaces
{
    public interface IDataStore
    {
        ISquareMatrix2D<int> Get();
        void Save(ISquareMatrix2D<int> matrix);
    }
}