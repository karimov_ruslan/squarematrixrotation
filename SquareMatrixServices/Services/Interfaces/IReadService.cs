﻿namespace SquareMatrixServices.Services.Interfaces
{
    public interface IReadService
    {
        public string ReadLine();
    }
}