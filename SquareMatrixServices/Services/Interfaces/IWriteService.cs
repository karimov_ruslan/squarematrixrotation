﻿namespace SquareMatrixServices.Services.Interfaces
{
    public interface IWriteService
    {
        public void WriteLine(object obj = null);
    }
}