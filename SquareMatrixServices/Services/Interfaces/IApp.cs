﻿namespace SquareMatrixServices.Services.Interfaces
{
    public interface IApp
    {
        void Startup();
    }
}