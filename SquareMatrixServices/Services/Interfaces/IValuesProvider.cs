﻿namespace SquareMatrixServices.Services.Interfaces
{
    public interface IValuesProvider<T>
    {
        public T Next();
    }
}