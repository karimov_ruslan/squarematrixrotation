﻿using Microsoft.Extensions.DependencyInjection;
using SquareMatrixServices.Services.Interfaces;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = DIService.GetProvider();
            var mainApp = serviceProvider.GetService<IApp>();
            mainApp.Startup();
        }
    }
}
