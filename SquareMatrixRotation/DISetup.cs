﻿using Microsoft.Extensions.DependencyInjection;
using SquareMatrixServices.Services;
using SquareMatrixServices.Services.Interfaces;

namespace TestConsole
{
    public static class DIService
    {
        public static ServiceProvider GetProvider() => 
            new ServiceCollection()
                .AddSingleton<IReadService, ConsoleReader>()
                .AddSingleton<IWriteService, ConsoleWriter>()
                .AddSingleton<IDataStore, FileDataStore>()
                .AddSingleton<IValuesProvider<int>, RandomIntegersProvider>(x =>
                    new RandomIntegersProvider(1, 1000))
                .AddSingleton<IMatrixFiller<int>, MatrixFiller<int>>()
                .AddSingleton<IMatrixSquare2DCircleTraversal, MatrixSquare2DCircleTraversal>()
                .AddSingleton<ISquare2DMatrixRotator, Square2DMatrixRotator>()
                .AddSingleton<IApp, SquareMatrixRotationApp>()
                .BuildServiceProvider();
    }
}
