﻿using Moq;
using Xunit;
using SquareMatrixServices.Models;
using SquareMatrixServices.Services;
using SquareMatrixServices.Services.Interfaces;
using System.IO;
using SquareMatrixServices.Models.Interfaces;

namespace SquareMatrixServices.Tests
{
    public class FileDataStoreTests
    {
        [Fact]
        public void Should_SaveToDisk()
        {
            // Arrange
            var fileName = "Test.csv";
            var dataStore = new FileDataStore(fileName);
            var matrix = GetZeroMatrixOf2();
            // Act
            dataStore.Save(matrix);

            // Assert
            var fileExist = File.Exists(fileName);
            var isNotEmpty = false;
            if (fileExist)
                isNotEmpty = !string.IsNullOrWhiteSpace(File.ReadAllText(fileName));

            Assert.True(fileExist, "File does not exist.");
            Assert.True(isNotEmpty, "File is empty.");

            if (fileExist)
                File.Delete(fileName);
        }

        [Fact]
        public void Should_ReadFromDisk()
        {
            // Arrange
            var fileName = "Test.csv";
            var dataStore = new FileDataStore(fileName);
            var matrix = GetZeroMatrixOf2();
            dataStore.Save(matrix);

            // Act
            var result = dataStore.Get();

            // Assert
            var matrixesEquals = MatrixesAreEqual(result, matrix);

            Assert.NotNull(result);
            Assert.True(matrixesEquals, "Read and source matrixes are different");

            File.Delete(fileName);
        }

        private bool MatrixesAreEqual<T>(IMatrix2D<T> first, IMatrix2D<T> second)
        {
            if (first.Rows != second.Rows || first.Columns != second.Columns)
                return false;

            for (int row = 0; row < first.Rows; row++)
                for (int column = 0; column < first.Columns; column++)
                    if (!Equals(first[row, column], second[row, column]))
                        return false;

            return true;
        }

        private ISquareMatrix2D<int> GetZeroMatrixOf2()
        {
            var matrix = new SquareMatrix2D<int>(2);
            matrix[0, 0] = 0;
            matrix[0, 1] = 0;
            matrix[1, 0] = 0;
            matrix[1, 1] = 0;
            return matrix;
        }
    }
}
