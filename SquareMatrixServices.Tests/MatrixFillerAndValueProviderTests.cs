﻿using Moq;
using Xunit;
using SquareMatrixServices.Models;
using SquareMatrixServices.Services;
using SquareMatrixServices.Services.Interfaces;
using SquareMatrixServices.Models.Interfaces;

namespace SquareMatrixServices.Tests
{
    public class MatrixFillerAndValueProviderTests
    {
        [Fact]
        public void Should_FillAllCells_Matrix3()
        {
            // Arrange

            var valuesProviderMock = new Mock<IValuesProvider<int>>();
            valuesProviderMock.Setup(x => x.Next())
                .Returns(() => 1);
            var filler = new MatrixFiller<int>(valuesProviderMock.Object);

            var matrix = GetZeroMatrixOf3();

            // Act
            filler.Fill(matrix);

            // Assert
            Assert.All(matrix, x => Assert.Equal(1, x));
        }

        [Fact]
        public void Should_GiveIntegersInRange_1_100()
        {
            // Arrange

            var valuesProvider = new RandomIntegersProvider(1, 100);

            // Act
            var value1 = valuesProvider.Next();
            var value2 = valuesProvider.Next();
            var value3 = valuesProvider.Next();

            // Assert
            Assert.InRange(value1, 1, 100);
            Assert.InRange(value2, 1, 100);
            Assert.InRange(value3, 1, 100);
        }

        private ISquareMatrix2D<int> GetZeroMatrixOf3()
        {
            var matrix = new SquareMatrix2D<int>(3);
            matrix[0, 0] = 0;
            matrix[0, 1] = 0;
            matrix[0, 2] = 0;
            matrix[1, 0] = 0;
            matrix[1, 1] = 0;
            matrix[1, 2] = 0;
            matrix[2, 0] = 0;
            matrix[2, 1] = 0;
            matrix[2, 2] = 0;
            return matrix;
        }
    }
}
