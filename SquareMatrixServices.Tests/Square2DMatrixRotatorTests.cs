using Moq;
using Xunit;
using SquareMatrixServices.Models;
using SquareMatrixServices.Services;
using SquareMatrixServices.Services.Interfaces;
using SquareMatrixServices.Models.Interfaces;

namespace SquareMatrixServices.Tests
{
    public class Square2DMatrixRotatorTests
    {        
        private static ISquare2DMatrixRotator GetRotator()
        {
            var traversal = new MatrixSquare2DCircleTraversal();
            var testingRotator = new Square2DMatrixRotator(traversal);
            return testingRotator;
        }

        [Fact]
        public void Should_Rotate90Degree_MatrixOf3()
        {
            // Arrange
            var testingRotator = GetRotator();

            var matrix = GetMatrixOf3();
            var matrixRotated90Degrees = GetRotated90MatrixOf3();

            // Act
            testingRotator.Rotate90Degree(matrix);

            // Assert

            Assert.True(MatrixesAreEqual(matrix, matrixRotated90Degrees), "Rotation was not valid");
        }

        [Fact]
        public void Should_RotateMinus90Degree_MatrixOf3()
        {
            // Arrange

            var testingRotator = GetRotator();

            var matrix = GetMatrixOf3();
            var matrixRotatedMinus90Degrees = GetRotatedMinus90MatrixOf3();

            // Act
            testingRotator.RotateMinus90Degree(matrix);

            // Assert

            Assert.True(MatrixesAreEqual(matrix, matrixRotatedMinus90Degrees), "Rotation was not valid");
        }

        [Fact]
        public void Should_Rotate90Degree_MatrixOf4()
        {
            // Arrange
            var testingRotator = GetRotator();

            var matrix = GetMatrixOf4();
            var matrixRotated90Degrees = GetRotated90MatrixOf4();

            // Act
            testingRotator.Rotate90Degree(matrix);

            // Assert

            Assert.True(MatrixesAreEqual(matrix, matrixRotated90Degrees), "Rotation was not valid");
        }

        [Fact]
        public void Should_RotateMinus90Degree_MatrixOf4()
        {
            // Arrange
            var testingRotator = GetRotator();

            var matrix = GetMatrixOf4();
            var matrixRotatedMinus90Degrees = GetRotatedMinus90MatrixOf4();

            // Act
            testingRotator.RotateMinus90Degree(matrix);

            // Assert
            Assert.True(MatrixesAreEqual(matrix, matrixRotatedMinus90Degrees), "Rotation was not valid");
        }

        [Fact]
        public void Should_GenerateNotNullString_MatrixOf3()
        {
            // Arrange
            var testingRotator = GetRotator();
            var matrix = GetMatrixOf3();

            // Act
            var result = testingRotator.LevelsToString(matrix, Enums.RotationDirection.AntiClockwise);

            // Assert

            Assert.NotNull(result);
        }

        private bool MatrixesAreEqual<T>(IMatrix2D<T> first, IMatrix2D<T> second)
        {
            if (first.Rows != second.Rows || first.Columns != second.Columns)
                return false;

            for (int row = 0; row < first.Rows; row++)
                for (int column = 0; column < first.Columns; column++)
                    if (!Equals(first[row, column], second[row, column]))
                        return false;

            return true;
        }

        private ISquareMatrix2D<int> GetMatrixOf3()
        {
            var matrix = new SquareMatrix2D<int>(3);
            matrix[0, 0] = 0;
            matrix[0, 1] = 1;
            matrix[0, 2] = 2;
            matrix[1, 0] = 3;
            matrix[1, 1] = 4;
            matrix[1, 2] = 5;
            matrix[2, 0] = 6;
            matrix[2, 1] = 7;
            matrix[2, 2] = 8;
            return matrix;
        }

        private ISquareMatrix2D<int> GetRotated90MatrixOf3()
        {
            var matrix = new SquareMatrix2D<int>(3);
            matrix[0, 0] = 6;
            matrix[0, 1] = 3;
            matrix[0, 2] = 0;
            matrix[1, 0] = 7;
            matrix[1, 1] = 4;
            matrix[1, 2] = 1;
            matrix[2, 0] = 8;
            matrix[2, 1] = 5;
            matrix[2, 2] = 2;
            return matrix;
        }

        private ISquareMatrix2D<int> GetRotatedMinus90MatrixOf3()
        {
            var matrix = new SquareMatrix2D<int>(3);
            matrix[0, 0] = 2;
            matrix[0, 1] = 5;
            matrix[0, 2] = 8;
            matrix[1, 0] = 1;
            matrix[1, 1] = 4;
            matrix[1, 2] = 7;
            matrix[2, 0] = 0;
            matrix[2, 1] = 3;
            matrix[2, 2] = 6;
            return matrix;
        }

        private ISquareMatrix2D<int> GetMatrixOf4()
        {
            var matrix = new SquareMatrix2D<int>(4);
            matrix[0, 0] = 0;
            matrix[0, 1] = 1;
            matrix[0, 2] = 2;
            matrix[0, 3] = 3;
            matrix[1, 0] = 4;
            matrix[1, 1] = 5;
            matrix[1, 2] = 6;
            matrix[1, 3] = 7;
            matrix[2, 0] = 8;
            matrix[2, 1] = 9;
            matrix[2, 2] = 10;
            matrix[2, 3] = 11;
            matrix[3, 0] = 12;
            matrix[3, 1] = 13;
            matrix[3, 2] = 14;
            matrix[3, 3] = 15;
            return matrix;
        }

        private ISquareMatrix2D<int> GetRotated90MatrixOf4()
        {
            var matrix = new SquareMatrix2D<int>(4);
            matrix[0, 0] = 12;
            matrix[0, 1] = 8;
            matrix[0, 2] = 4;
            matrix[0, 3] = 0;
            matrix[1, 0] = 13;
            matrix[1, 1] = 9;
            matrix[1, 2] = 5;
            matrix[1, 3] = 1;
            matrix[2, 0] = 14;
            matrix[2, 1] = 10;
            matrix[2, 2] = 6;
            matrix[2, 3] = 2;
            matrix[3, 0] = 15;
            matrix[3, 1] = 11;
            matrix[3, 2] = 7;
            matrix[3, 3] = 3;
            return matrix;
        }
        
        private ISquareMatrix2D<int> GetRotatedMinus90MatrixOf4()
        {
            var matrix = new SquareMatrix2D<int>(4);
            matrix[0, 0] = 3;
            matrix[0, 1] = 7;
            matrix[0, 2] = 11;
            matrix[0, 3] = 15;
            matrix[1, 0] = 2;
            matrix[1, 1] = 6;
            matrix[1, 2] = 10;
            matrix[1, 3] = 14;
            matrix[2, 0] = 1;
            matrix[2, 1] = 5;
            matrix[2, 2] = 9;
            matrix[2, 3] = 13;
            matrix[3, 0] = 0;
            matrix[3, 1] = 4;
            matrix[3, 2] = 8;
            matrix[3, 3] = 12;
            return matrix;
        }
    }
}
