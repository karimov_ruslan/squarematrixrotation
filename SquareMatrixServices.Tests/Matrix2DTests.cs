﻿using Xunit;
using SquareMatrixServices.Models;
using SquareMatrixServices.Models.Interfaces;
using System.Linq;

namespace SquareMatrixServices.Tests
{
    public class Matrix2DTests
    {
        [Fact]
        public void Should_EnumeratedSumBeEqualToExpected()
        {
            // Arrange
            var matrix = GetOneMatrixOf2();

            // Act
            var result = matrix.Sum();

            // Assert
            Assert.Equal(4, result);
        }

        [Fact]
        public void Should_CopyMatrixes()
        {
            // Arrange
            var matrix0 = GetZeroMatrixOf2();
            var matrix1 = GetOneMatrixOf2();

            //Act
            matrix0.CopyTo(matrix1);

            // Assert
            var copiedMatrixEqualsToOriginal = MatrixesAreEqual(matrix0, matrix1);

            Assert.True(copiedMatrixEqualsToOriginal, "Copied matrix is not equal to source!");
        }

        private ISquareMatrix2D<int> GetOneMatrixOf2()
        {
            var matrix = new SquareMatrix2D<int>(2);
            matrix[0, 0] = 1;
            matrix[0, 1] = 1;
            matrix[1, 0] = 1;
            matrix[1, 1] = 1;
            return matrix;
        }

        private bool MatrixesAreEqual<T>(IMatrix2D<T> first, IMatrix2D<T> second)
        {
            if (first.Rows != second.Rows || first.Columns != second.Columns)
                return false;

            for (int row = 0; row < first.Rows; row++)
                for (int column = 0; column < first.Columns; column++)
                    if (!Equals(first[row, column], second[row, column]))
                        return false;

            return true;
        }

        private ISquareMatrix2D<int> GetZeroMatrixOf2()
        {
            var matrix = new SquareMatrix2D<int>(2);
            matrix[0, 0] = 0;
            matrix[0, 1] = 0;
            matrix[1, 0] = 0;
            matrix[1, 1] = 0;
            return matrix;
        }
    }
}
